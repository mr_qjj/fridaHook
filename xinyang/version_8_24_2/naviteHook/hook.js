
/**
 * hook导出的加密函数的参数,
 * so里面共传入四个参数，校验传入数据
 * @type {NativePointer}
 */

const calculate = Module.findExportByName("libsyoput.so", "Java_oO_oz_o0s_o_O0_o0o")
if (calculate !== null) {
    Interceptor.attach(calculate, {
        onEnter: function (args) {
            let byteArray = args[3].readByteArray(200);
            let hexdata = hexdump(byteArray, {
                ansi: true,
                length: 200,
            });
            console.log(hexdata);
            this.retbuff = args[3]
        },
        onLeave: function (args) {
            const ret = this.retbuff.readByteArray(200);
            const hexdata = hexdump(ret, {
                ansi: true,
                length: 200,
            });
            console.log("onLeave---------------------------------------------------------------------------------------------------------------------")
            console.log(hexdata);
        }
    });
}