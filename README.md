# fridaHook 欢迎Fork项目,共同沉淀,相互学习
### 启动方式 frida -U -f com.xxx.xxx -l hook.js --no-pause
### 参数可以参考官方 -U 为USB 多设备使用-D 指定设备, -f 为杀死进程启动,-n 为附加进程
### 目录结构为: 

```
|--->projectName
|------>"version"_versionNum_author
|------------>javaHook
|------------------>Hook.js
|------------>naviteHook
|------------------>Hook.js
|------------>README.md
```
