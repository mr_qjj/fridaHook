/**
 * @project: fridahook
 * @package:
 * @Description:
 * @Author: ts.
 * @Email:
 * @Date: 2021-10-25 11:28:33
 */


function showStacks() {
    Java.perform(function () {
        console.log("----------------------------------------------------------------------------------------------\n");
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        console.log("----------------------------------------------------------------------------------------------\n");
    });
}

// Java.perform(function () {
//     let inteceptor = Java.use("com.bk.base.netimpl.interceptor.d");
//     inteceptor.signRequest.implementation = function (v1, v2) {
//         let res = this.signRequest(v1, v2);
//         console.log("签名结果: " + res);
//         return res;
//     }
// });

Java.perform(function () {
    let getSig = Java.use("com.bk.base.netimpl.a");
    getSig.getSignString.implementation = function (v1, v2) {
        console.log("参数1： " + v1);
        var it = v2.keySet().iterator();
        let result = "";
        while(it.hasNext()){
            var keystr = it.next().toString();
            var valuestr = v2.get(keystr).toString();
            result += keystr + valuestr + "        ";
        }
        console.log("参数2： " + result);
        return this.getSignString(v1,v2);
    }

    //以下两个函数没打印东西, 直接hook JNI 的函数
    let api = Java.use("com.bk.base.router.ModuleRouterApi");
    api.getHttpAppSecret = function (){
        console.log("getHttpAppSecret" + api.getHttpAppSecret());
        return api.getHttpAppSecret();
    }

    api.getHttpAppId = function () {
        console.log("getHttpAppId" + api.getHttpAppId());
        return api.getHttpAppId();
    }

    let jni = Java.use("com.homelinkndk.lib.JniClient");
    jni.GetAppId.implementation = function (v1) {
        let res  = this.GetAppId(v1);
        console.log("appId Jni param1: " + v1);
        console.log("appId Jni: " + res);
        return res;
    }

    let log = Java.use("com.bk.base.util.bk.LjLogUtil");
    log.d.overload('java.lang.String', 'java.lang.String').implementation = function (v1,v2){
        console.log("Log.d(): " + v1,v2);
    }



    let encrypt = Java.use("com.bk.base.util.bk.DeviceUtil");
    encrypt.SHA1ToString.implementation = function (v1) {
        let res = this.SHA1ToString(v1)
        console.log("加密参数为: " + v1 + "加密结果为: " + res)
        return res;
    }

});
