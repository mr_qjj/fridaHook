/**
 * 通过分析代码,先从navite函数获取了一个String字符串,
 * 这个字符串应该是跟加密有关系的,所以我们要得到他的返回值(我觉得这个值应该是个死的或者固定的),
 * 看看是个什么东西,然后下一步根据这个返回值判断操作处理,
 * 然后传入navite的加密函数
 */
Java.perform(function (){
    const n = Java.use("com.duapp.aesjni.AESEncrypt");
    n.getByteValues.implementation = function(){
        /**
         * 这里的返回值都是一致的应该是key的二进制类的东西
         * 从so层获取到的返回值为:101001011101110101101101111100111000110100010101010111010001000101100101010010010101110111010011101001011101110101100101001100110000110100011101010111011011001101001
         *     public static String b(Object obj, String str) {
                String byteValues = getByteValues();  //这里调用获取的二进制的数据 就是上面的结果
                StringBuilder sb = new StringBuilder(byteValues.length());
                //这个循环是做了上面二进制字符串的取反操作
                for (int i2 = 0; i2 < byteValues.length(); i2++) {
                    if (byteValues.charAt(i2) == '0') {
                        sb.append('1');
                    } else {
                        sb.append('0');
                    }
                }
                //传入加密参数,hook
                return encodeByte(str.getBytes(), sb.toString());
            }
         */
        const res = n.getByteValues();
        // console.log("从so层获取到的返回值为:" + res);
        return res;
    }

    /**
     * 这个类下的b函数 有两个参数,参数1是个Object类型,参数2是个String类型，应该是要加密的数据
     * encodeByte(str.getBytes(), sb.toString()); 最终调用了这个函数,str是要加密的字符串，sb.toString()就是取反的二进制
     * 要加密的数据为:loginTokenplatformandroidtimestamp1611231304015uuid26f970aff39fe635v4.62.2
     * 返回结果为: knGGXR0bR7LQn4eRCvJsdZ4D96wrRcYi2zPWWxLMOs3aoZ1qYh18IBY5KSef3W8X13ZNQTTO2R9buZBeuktDaA0cWpUZKL7O5Y77jVOjSxQ=
     * 再次使用md5 对结果在加密一下,写入请求中的newSign字段参与post提交
     * @param v1
     * @param v2
     */
    n.b.implementation = function(v1,v2){
        console.log("要加密的数据为:" + v2);
        let res = n.b(v1,v2);
        console.log("返回结果为: " + res)
        return res;
    }
})