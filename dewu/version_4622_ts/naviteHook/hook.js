
/**
 * hook导出的加密函数的参数,
 * so里面共传入两个参数,参数1 为要加密的内容, 参数2 为AES ECB模式的key
 * @type {NativePointer}
 */

const AES_Calculate = Module.findExportByName("libJNIEncrypt.so", "AES_128_ECB_PKCS5Padding_Encrypt")
if (AES_Calculate !== null) {
    Interceptor.attach(AES_Calculate, {
        onEnter: function (args) {
            let byteArray = args[0].readByteArray(200);
            let hexdata = hexdump(byteArray, {
                ansi: true,
                length: 200,
            });
            console.log(hexdata);
            console.log("============================================================================");
            byteArray = args[1].readByteArray(16);
            hexdata = hexdump(byteArray, {
                ansi: true,
                length: 16,
            });
            console.log(hexdata);
            console.log("============================================================================");
            this.retbuff = args[1]
        },
        onLeave: function (args) {
            // const ret = this.retbuff.readCString();
            // console.log("result", ret);
        }
    });
}