/**
 * hook Java 类
 * com.meitu.secret.SigEntity 签名实体结果类函数,与Navite层交互
 */
setImmediate(function () {
    if (Java.available) {
        Java.perform(function () {
            const SigEntity = Java.use("com.meitu.secret.SigEntity");

            // com.meitu.secret.SigEntity.generatorSig(java.lang.String, java.lang.String[], java.lang.String) : com.meitu.secret.SigEntity
            // Canonical: Lcom/meitu/secret/SigEntity;->generatorSig(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/meitu/secret/SigEntity;
            SigEntity.generatorSig.overload('java.lang.String', '[Ljava.lang.String;', 'java.lang.String').implementation = function (data1, data2, data3) {
                console.log(data1, data2, data3);
                return this.generatorSig(data1, data2, data3);
            }

            // com.meitu.secret.SigEntity.generatorSig(java.lang.String, java.lang.String[], java.lang.String, java.lang.Object) : com.meitu.secret.SigEntity
            // Canonical: Lcom/meitu/secret/SigEntity;->generatorSig(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lcom/meitu/secret/SigEntity;
            SigEntity.generatorSig.overload('java.lang.String', '[Ljava.lang.String;', 'java.lang.String', 'java.lang.Object').implementation = function (data1, data2, data3, data4) {
                console.log(data1, data2, data3, data4);
                return this.generatorSig(data1, data2, data3, data4);
            }

            // com.meitu.secret.SigEntity.generatorSigWithFinal(java.lang.String, java.lang.String[], java.lang.String, java.lang.Object) : com.meitu.secret.SigEntity
            // Canonical: Lcom/meitu/secret/SigEntity;->generatorSigWithFinal(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lcom/meitu/secret/SigEntity;
            SigEntity.generatorSigWithFinal.implementation = function (data1, data2, data3, data4) {
                console.log('generatorSigWithFinal', data1, data2, data3, data4);
                const result = this.generatorSigWithFinal(data1, data2, data3, data4);

                console.log("generatorSigWithFinal", result.sig.value, result.sigTime.value, result.sigVersion.value);

                return result;
            }
        });
    }
});