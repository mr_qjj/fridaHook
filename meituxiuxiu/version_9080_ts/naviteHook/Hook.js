
/**
 * hook 导出函数_Z13MD5_CalculatePKcjPc验证参数
 * @type {NativePointer}
 */
const MD5_Calculate = Module.findExportByName("librelease_sig.so", "_Z13MD5_CalculatePKcjPc")
if (MD5_Calculate !== null) {
    Interceptor.attach(MD5_Calculate, {
        onEnter: function (args) {
            const byteArray = args[0].readByteArray(args[1].toInt32());
            const hexdata = hexdump(byteArray, {
                ansi: true,
                length: args[1].toInt32(),
            });
            console.log(hexdata);
            this.retbuff = args[2]
        },
        onLeave: function (args) {
            const ret = this.retbuff.readCString();
            console.log("result", ret);
        }
    });
}