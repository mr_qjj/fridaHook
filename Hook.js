/**
 * @project: fridahook
 * @package:
 * @Description:
 * @Author: ts.
 * @Email:
 * @Date: 2021-12-24 16:00:08
 */


function showStacks() {
    Java.perform(function () {
        console.log("----------------------------------------------------------------------------------------------\n");
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        console.log("----------------------------------------------------------------------------------------------\n");
    });
}


Java.perform(function () {
    const encrypt = Java.use("com.autonavi.server.aos.serverkey");
    const res = encrypt.amapDecode("upW9DSFhG5+AmBgNUUJKu3Nfp5WNV31C7yjuXhTEQmtAOIPVw66h07Cv/rqoBDOtibSfOl9Lg2YBBOclCnQavJdqJDPixBzg7HCoatt8kUBQQrW7kHH6ekLhY0jjkhYphlMr//XzWnOtrwH/5DQMQGMkF+u6JehTcsJRhmAFpJT/OFZ03RPYchV0O4bPMJd2sum93Z79xTnaA9gjxxSbnvzGwm8nISWzOLVgcW7tZZVlmTMQpLDP37yuQSRhINmU6aMpaMNNCnQTmDgR9VOZ6mY8DBbUMAjYl8nFqGU2RDh6iiKB3sTa9LTukWET5vAkhgja9iOKdxcCm8CpjOLy6BFKxg5KrEDsDF+VIvlq5tqcx13eSHiBq9KwzROtr4GfomP1poodDU3C5u/EwKuR4QRBXZklkJhs5rJfJ/r+PYcrBnSYJA2L9o0H3o21Tu2SE0EqLaPsSYPZkJC/DwLWBM6zV3UD8dwoRtpXaRpuHGzjh05TqhavetYBHdpLs7/5eMeXVljrB9YXZ3Cvcg5IBXlPJXDv99VU67qWf1+T67GM3GmLMNAP6V0wAMKloCxNsVBTDZ7ascZmd1nU3eYQINg/7qex/6dyI8fkBZUcQPfr1YxCCniokv6VvnJA2jkZFWgwRmC8HbJqYXNW33HWO/t/iKx/PMahSc7CcJWOPKilttjzOURiS5vrfvD5OCulkQsOhQtWZEUnCKtAI5J4fZUaRC93pZs2RVGBbPiLkjvF/Mrcs+oGJHEp8XhycEBiW+Pf2DTbo6uY7CPpVDBgO1J5aO4hfq+ZoQiKO0TLOGZZmKOrIcv+VMD1CF4+9Hi3JygIRK7Wq88zhCv3d6z8vX6aePv2fwEdfzWXSBO8ptww2gEPUnbghPwA7blV8m16Hq7zq/uZfW+0noIA+wSPHOH45Ov+a9X70hcnWXIu7V5cGXO6ZdhduUr9WpBdoqAjp3Ztjw+1Fs+aJBAiQpuhXQCXHtpcoz2i1+8t9PFt8uw/apK79EqGvEKGX2/vZhGUMFvT5mCJNW43WnU4sKJAVuZiTzJwcDfs4EQc2fqbRrTKkZG78FWerNs36TLrigymRywog+L3Jja4XSe5LhLVGCGI84ehzmNVOApV4ul0l7RX9K7Pcp76qBB1FfCtdnl6taI/gSkMdKhBZ6jdLhyVhEtYovysDLsICbdwNDJ6/pg5JCKYdl6pPquzlaOOSP4MT/RcjwFBeDEGwxgthDb4ghalcChfCayJbuZ8vDHReO0BJTw49Uq3dHN5WTLHLiX4W2nm7T4+vcrx/ECJVSnxmzfTK5nqhKiXGUWe5oVHxbHgNJE96qsxGSCkv+GmgcuTdUqWVVVGc+bjimUpRbgiXgZHY2XHcXCEbSIJ5TUjy5BF5usYLcPxfqoWqPy2UF1vt4vt6GiBCOsh+tWCDGvq3oUhYTWq3O4jb+FHFeYvhWEIWiY6l4p0VkSiT5lqqciZFcgZY8WuDPYyviE5PD96trBeBn674K7H56OIK9A7qoBoWhzwBTZFH3Cz69dbqLZkNVpgRw==")
    // console.log(encrypt.amapEncode("ct_id=9001_b&session=351702195&channel=amap7a&sign=5241B1FA6AF1F740E7A4CCAF53EA0448&dip=10880&diu3=090001fc20bf4786ab45fbb74091ebba-0c5c917b759107d1c1eb901a121baecb&diu2=fffffffffffdffffffe8&dai=6a4a019c7c50c256&ajxVersion=nearby%3A053155%3Bscenic_area%3A052018%3Bpoi%3A053155%3Btravel%3A050120%3Bwalkman%3A052018%3Bidqplus%3A052007%3Bidqmax%3A052018%3Bsearch%3A053155%3Bsearch_around%3A050300%3Bhotel%3A053155%3Blanding_page%3A052040%3Btour%3A050815%3Bsearch_cloud%3A052743%3Bpoi_cloud%3A053155%3Bfavorites%3A053017%3Bnearby_cloud%3A052007%3Bcomment%3A052040%3Border_center%3A052018%3Binformation_im%3A051716&tid=YHOvpm2DhQ8DAJakOAOxjvf0&cifa=38043E0854F6F006DEFA6002010001CC0100007AA00000F5DD1200000000000000000000000000000000001D0000000A004D323030334A3135534306006D65726C696E06005869616F6D690C0031312E31332E302E32393235000000000000000000006400&appstartid=351702195&div=ANDH111300&output=json&diu=&uid=&oaid=edb64b3a5004567c&BID_F=M090000&siv=ANDH111300&adiu=ic6jgi7laf669gadcg5jgab0a0472b&buildABI=arm64-v8a&stepid=714&modules=%5B%22surround_facility%22%5D&spm=1033274041545503823503&dibv=2925&poiid=B000A7HYBC&client_network_class=4&dic=C3300&aetraffic=9"))
    console.log("\n")
    console.log(res)

    const dig = Java.use("java.security.MessageDigest")
    dig.digest.overload().implementation = function () {
        // console.log(v1);
        const result = this.digest();
        var cla = Java.use("com.amap.bundle.utils.encrypt.HexUtil");
        var r = cla.a(result)
        if (r == "5241B1FA6AF1F740E7A4CCAF53EA0448"){
            console.log(r);
            showStacks();
        }
        return result;
    }

    // const hex = Java.use("com.amap.bundle.utils.encrypt.HexUtil");
    // hex.a.implementation = function (v1) {
    //     const result = this.a(v1);
    //     console.log(result)
    //     return result;
    // }
});