/**
 * @project: fridahook
 * @package:
 * @Description:
 * @Author: ts.
 * @Email: 
 * @Date: 2021-03-22 13:58:17
 */



/**
 * 打印堆栈信息。
 */
function showStacks() {
    Java.perform(function () {
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
    });
}


Java.perform(function () {
        const u = Java.use("com.jingdong.common.utils.BitmapkitUtils");
    /**
     * 京东签名加密函数
     * @param v1
     * @param v2
     * @param v3
     * @param v4
     * @param v5
     * @param v6
     * @returns {*}
     */
        u.getSignFromJni.overload('android.content.Context', 'java.lang.String', 'java.lang.String', 'java.lang.String', 'java.lang.String', 'java.lang.String').implementation = function (v1, v2, v3, v4, v5, v6) {
            console.log("======================================================================================")
            showStacks();
            console.log("v2:\t"+v2);
            console.log("v3:\t"+v3);
            console.log("v4:\t"+v4);
            console.log("v5:\t"+v5);
            console.log("v6:\t"+v6);
            let res = u.getSignFromJni(v1,v2,v3,v4,v5,v6);
            console.log("res:\t"+ res);
            console.log("======================================================================================")
            return res;
        }
    })