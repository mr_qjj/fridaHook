Java.perform(function(){
        var c;
        var application = Java.use("android.app.Application");
        var reflectClass = Java.use("java.lang.Class");
        application.attach.overload('android.content.Context').implementation = function(context) {
            var result = this.attach(context); // 先执行原来的attach方法
            var classloader = context.getClassLoader(); // 获取classloader
            Java.classFactory.loader = classloader;
            var ec = Java.classFactory.use("eb.c")
            ec.b.overload('java.lang.String').implementation = function(v1){
                console.log("参数值为:"+v1);
                var res =  this.b(v1);
                console.log("返回加密结果:"+res);
                return res;
            }
//             c = classloader.loadClass("eb.c"); // 使用classloader加载类
//             c = Java.cast(c, reflectClass); // 因为loadClass得到的是一个Object对象，我们需要把它强制转换成Class
//             console.log("AyWelcome class name: " + c.getName());
            return result;
        }
});