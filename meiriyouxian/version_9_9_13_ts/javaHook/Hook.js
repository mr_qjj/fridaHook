/**
 * @project: fridahook
 * @package:
 * @Description: 每日优鲜 mfsig hook 目标APP版本 9.9.13
 * @Author: ts.
 * @Email: 
 * @Date: 2021-02-25 14:44:13
 */

function showStacks() {
    Java.perform(function () {
        console.log("----------------------------------------------------------------------------------------------\n");
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        console.log("----------------------------------------------------------------------------------------------\n");
    });
}


/**
 * hook 每日优先签名数据
 */
Java.perform(function(){
    var c;
    var application = Java.use("android.app.Application");
    var reflectClass = Java.use("java.lang.Class");
    application.attach.overload('android.content.Context').implementation = function(context) {
        var result = this.attach(context); // 先执行原来的attach方法
        var classloader = context.getClassLoader(); // 获取classloader
        Java.classFactory.loader = classloader;
        var SecurityLib = Java.classFactory.use('cn.missfresh.wsg.SecurityLib')
        SecurityLib.nativeSign.implementation = function (v1,v2,v3) {
            console.log('v1   >>>>', v1);
            console.log('v2   >>>>', v2);
            console.log('v3   >>>>', Java.use("java.lang.String").$new(v3));
            var value = SecurityLib.nativeSign.call(this,v1,v2,v3);
            console.log('value   >>>',value)
            return value

        };
//             c = classloader.loadClass("eb.c"); // 使用classloader加载类
//             c = Java.cast(c, reflectClass); // 因为loadClass得到的是一个Object对象，我们需要把它强制转换成Class
//             console.log("AyWelcome class name: " + c.getName());
        return result;
    }
});