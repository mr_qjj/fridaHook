/**
 * @project: fridahook
 * @package:
 * @Description:
 * @Author: ts.
 * @Email: 
 * @Date: 2021-04-12 15:52:46
 */

function showStacks() {
    Java.perform(function () {
        console.log("----------------------------------------------------------------------------------------------\n");
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        console.log("----------------------------------------------------------------------------------------------\n");
    });
}

Java.perform(function () {
    // HOOK url函数,可以得到请求之前的调用链
    // let url = Java.use("java.net.URL");
    // url.$init.overload('java.lang.String').implementation = function (v1){
    //     if (v1.indexOf("wsgsig") > 0){
    //         console.log(v1);
    //         showStacks();
    //     }
    //     return this.$init(v1);
    // }

    let f = Java.use("didihttp.HttpUrl");
    f.$init.implementation = function (v1) {
        if(v1.toString().indexOf("getGoodDetail") > 0){
            console.log("param1 ---------------_>" +  v1.toString());
            showStacks();
        }

    }
})