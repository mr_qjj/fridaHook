/**
 * @project: fridahook
 * @package:
 * @Description:
 * @Author: ts.
 * @Email: 
 * @Date: 2021-05-12 14:36:43
 */

function showStacks() {
    Java.perform(function () {
        console.log("----------------------------------------------------------------------------------------------\n");
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        console.log("----------------------------------------------------------------------------------------------\n");
    });
}



Java.perform(function () {
    // Hook url 初始化类
    const url = Java.use("java.net.URL");
    url.$init.overload('java.lang.String').implementation = function (v1) {
        //这里判断一下接口请求地址是否包含xxxxxxxxx
        if (v1.indexOf("/mapi/wedding/homeshopinfo.bin") > 0){
            console.log(v1);
            showStacks();
        }
        return this.$init(v1);
    }


    //HOOK 生成接口实现类的函数, 在通过反射可以直接调用方法，传参即可。
    const c = Java.use("com.sankuai.meituan.retrofit2.Retrofit")
    c.create.implementation = function (v1) {
        console.log(v1)
        return this.create(v1);
    }
})
