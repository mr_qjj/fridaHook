/**
 * @project: fridahook
 * @package:
 * @Description:
 * @Author: ts.
 * @Email: 
 * @Date: 2021-04-12 15:52:46
 */

function showStacks() {
    Java.perform(function () {
        console.log("----------------------------------------------------------------------------------------------\n");
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        console.log("----------------------------------------------------------------------------------------------\n");
    });
}

Java.perform(function () {
    //
    // //上层调用加密函数(直接传入url即可返回mtgsig参数), 该函数开放远程调用无效,加密在so内获取了用户的令牌t 和设备id 和userid参与加密
    // let s2 = Java.use("com.meituan.android.common.mtguard.wtscore.plugin.sign.core.CandyPreprocessor")
    // s2.makeHeader.implementation = function (v1){
    //     console.log("========================================================================================")
    //     console.log("v1: " + v1.toString());
    //     let res = this.makeHeader(v1);
    //     console.log("sig: " + res);
    //     showStacks();
    //     console.log("========================================================================================")
    //     return res;
    // }


    //Hook 获取mtgsig的函数返回值为URI, 登录后直接调用这个函数，传入url 即可返回最终的header数据 包括 mtgsig mtgdfpid siua
    // let g = Java.use("com.meituan.android.common.mtguard.wtscore.plugin.sign.core.AddSigUtils");
    // g.get.overload('java.net.URI', 'java.lang.String', 'java.lang.String', 'java.util.Map').implementation=function (v1, v2, v3, v4) {
    //     console.log(v1);
    //     console.log(v2);
    //     console.log(v3);
    //     console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
    //     let res = g.get(v1,v2,v3,v4);
    //     let keyset = v4.keySet();
    //     let it = keyset.iterator();
    //     while (it.hasNext()){
    //         let keystr = it.next().toString();
    //         let valuestr = v4.get(keystr).toString();
    //         console.log("key: " + keystr + "\tvalues: " + valuestr);
    //     }
    //     console.log(res)
    //     console.log("========================================================================================================================================");
    //     return res;
    // }



    // //接近navite函数
    // let s3 = Java.use("com.meituan.android.common.mtguard.wtscore.plugin.sign.core.WTSign")
    // s3.makeHeader.implementation = function (v1,v2) {
    //     console.log("----------------------------------------------------------------------------------------")
    //     console.log(Java.use("java.lang.String").$new(v1).toString())
    //     let res = this.makeHeader(v1,v2);
    //     console.log("res: " + res)
    //     return res;
    //     console.log("----------------------------------------------------------------------------------------")
    // }

    // HOOK url函数,可以得到请求之前的调用链
    // let url = Java.use("java.net.URL");
    // url.$init.overload('java.lang.String').implementation = function (v1){
    //     if (v1.indexOf("api/c/poi") > 0){
    //         console.log(v1);
    //         showStacks();
    //     }
    //     return this.$init(v1);
    // }

    //hook 屏蔽掉root检测
    // let deviceUtils = Java.use("com.meituan.metrics.util.DeviceUtil");
    // deviceUtils.b.overload().implementation=function () {
    //     let res = deviceUtils.b();
    //     console.log("deviceUtils:  " + res)
    //     return false;
    // }
    //hook 支付环境 root 检测
    // let pay = Java.use("com.meituan.android.paybase.utils.v");
    // pay.a.overload().implementation = function () {
    //     let res = pay.a();
    //     console.log("paybase:  " + res);
    //     return false;
    // }
    //xposed环境检测,调用了腾讯的sdk
    // let posed = Java.use("com.tencent.smtt.sdk.WebView");
    // posed.getCrashExtraMessage.implementation =function (v1) {
    //     let res = posed.getCrashExtraMessage(v1);
    //     console.log(res);
    //     return res;
    // }


    let su = Java.use("com.meituan.android.common.fingerprint.FingerprintManager")
    su.root.implementation = function (){
        let res = su.root();
        console.log("res --------------->  " + res);
        return 0;
    }
})