/**
 * @project: fridahook
 * @package:
 * @Description: 慢慢买Token 调试
 * @Author: ts.
 * @Email: 
 * @Date: 2021-08-09 17:29:23
 */


/**
 * 打印堆栈信息。
 */
function showStacks() {
    Java.perform(function () {
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
    });
}


/**
 * 开始Hook 函数
 */
Java.perform(function () {
    const application = Java.use("android.app.Application");
    application.attach.overload('android.content.Context').implementation = function (context) {
        const result = this.attach(context); // 先执行原来的attach方法
        const classloader = context.getClassLoader(); // 获取classloader
        Java.classFactory.loader = classloader;
        const MD5Util = Java.use("com.maochunjie.mencryptsign.MD5Util");
        MD5Util.getMD5String.overload("java.lang.String").implementation = function(param1) {
            console.log("getMD5String.args: "  + param1);
            const result = this.getMD5String(param1);
            console.log("getMD5String.result : "  + result);
            return result
        }
        const RNReactNativeMencryptSignModule = Java.use("com.maochunjie.mencryptsign.RNReactNativeMencryptSignModule");
        RNReactNativeMencryptSignModule.getToken.overload("java.lang.String").implementation = function(param1) {
            console.log("getToken.args: "  + param1);
            const result = this.getToken(param1);
            return result
        }
        return result;
    }
});