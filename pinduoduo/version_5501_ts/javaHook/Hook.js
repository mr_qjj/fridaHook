/**
 * @project: fridahook
 * @package: pinduoduo
 * @Description: 拼多多hook脚本
 * @Author: ts, Ts.
 * @Email: 
 * @Date: 2021-02-07 15:32:45
 */


/**
 * 打印堆栈信息。
 */
function showStacks() {
    Java.perform(function () {
        // send(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
    });
}


Java.perform(function () {
    /**
     * anti-Token 生成函数传入请求的url地址
     * @type {Java.Wrapper<{}>}
     */
    const d = Java.use("com.aimi.android.common.http.a");
    d.a.overload('java.lang.String', 'boolean').implementation = function (v1, v2){
        const res = this.a(v1,v2);
        if (v1.indexOf("mall_combination") > 0){
            console.log(v1);
            console.log(v2);
            const keyset = res.keySet();
            const it = keyset.iterator();
            while(it.hasNext()){
                const keystr = it.next().toString();
                const valuestr = res.get(keystr).toString();
                console.log("key: " + keystr + "\t" + "value: " + valuestr)
            }
        }
        return res;
    }

    // var g = Java.use("com.xunmeng.pinduoduo.patch.g");
    // g.a.overload('java.lang.Throwable').implementation = function (v1){
    //     return false;
    //
    // }
})