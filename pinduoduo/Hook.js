/**
 * @project: fridahook
 * @package: 
 * @Description: 
 * @Author: ts.
 * @Email: 
 * @Date: 2022-01-21 11:47:10
 */


/**
 * 打印堆栈信息。
 */
function showStacks() {
    Java.perform(function () {
        // send(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
    });
}

Java.perform(function () {
    // let url = Java.use("java.net.URL");
    // url.$init.overload('java.lang.String').implementation = function (v1){
    //         console.log(v1);
    //         showStacks();
    //     return this.$init(v1);
    // }

    // let u = Java.use("com.xunmeng.pinduoduo.friend.b.a")
    // u.t.implementation = function () {
    //     var res = this.t();
    //     console.log(res)
    //     showStacks();
    //     return res;
    // }
    var ag = Java.use("okhttp3.ag")
    ag.n.implementation = function () {
        var res = this.n();
        console.log("n(): " + res)
        return res;
    }
    // let a = Java.use("okhttp3.ah")
    // a.n.implementation = function () {
    //     var r = this.n()
    //     console.log("r(): "+ r);
    //     return r;
    // }
})