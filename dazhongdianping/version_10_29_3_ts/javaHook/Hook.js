/**
 * @project: fridahook
 * @package:
 * @Description: 大用点评post数据 设备激活 验证
 * @Author: ts.
 * @Email: 
 * @Date: 2021-02-08 13:58:02
 */




/**
 * 打印堆栈信息。
 */
function showStacks() {
    Java.perform(function () {
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
    });
}


/**
 * 开始hook
 */
Java.perform(function () {
    // const i = Java.use("com.dianping.monitor.i")
    // i.a.overload('[B').implementation = function (v1) {
    //     const p1 = Java.use("java.lang.String").$new(v1);
    //     console.log("传入的byte参数为: " + p1);
    //     return i.a(v1);
    // }

    // Hook url 初始化类
    // const url = Java.use("java.net.URL");
    // url.$init.overload('java.lang.String').implementation = function (v1) {
    //     console.log(v1);
    //     if (v1.indexOf("devicedata") > 0){
    //         showStacks();
    //
    //     }
    //     return this.$init(v1);
    // }
    /**
     * Hook 获取详细请求地址
     * @type {Java.Wrapper<{}>}
     */
    const f = Java.use("com.dianping.nvnetwork.f$5");
    f.getCommand.implementation = function (v1) {
        console.log("参数为:   " + v1)
        console.log("当前url地址为:" + v1);
        if (v1.indexOf("devicedata.bin") > 0){
            showStacks();
        }
        return this.getCommand(v1);
    }

    /**
     * POST 数据
     * @type {Java.Wrapper<{}>}
     */
    // const post = Java.use("com.dianping.monitor.impl.a");
    // post.send.overload('com.dianping.monitor.impl.e').implementation = function (v1) {
    //     console.log("post l 字段的数据为:    " + v1.l.value);
    //     return this.send(v1);
    // }

    /**
     * hook 设备信息实体类
     * @type {Java.Wrapper<{}>}
     */
    const dev = Java.use("com.dianping.apimodel.DevicedataBin");
    dev.b.implementation = function () {
        const res = this.b();
        console.log("post数据为: " + this.b.value)
        return res;
    }

    /**
     * Hook 获取请求参数
     * @type {Java.Wrapper<{}>}
     */
    // const apiM = Java.use("com.dianping.util.p$1");
    // apiM.call.implementation = function (v1) {
    //     this.call(v1);
    //     showStacks();
    //     console.log(v1);
    // }
    /**
     * hook获取设备信息函数
     * @type {Java.Wrapper<{}>}
     */
    // const device = Java.use("com.meituan.android.common.unionid.oneid.util.DeviceInfo");
    // device.getInstance.implementation = function (v1) {
    //     console.log("获取设备信息数据开始打印异常:")
    //     showStacks();
    //     return device.getInstance(v1)
    // }

    /**
     * hook base64 函数
     * @type {Java.Wrapper<{}>}
     */
    const b64 = Java.use("android.util.Base64");
    b64.encodeToString.overload('[B', 'int').implementation = function (v1,v2){
        const res = this.encodeToString(v1,v2);
        console.log("编码前的数据为:" + Java.use("java.lang.String").$new(v1));
        console.log("编码后的数据为: " + res);
        showStacks();
        return "";
    }

    // const encode = Java.use("com.meituan.uuid.c");
    // encode.a.overload('java.lang.String', 'java.lang.String').implementation = function (v1, v2) {
    //     console.log("加密数据为: " + v1);
    //     console.log("key为: " + v2)
    //     return "";
    // }


    // const encode = Java.use("com.meituan.android.common.fingerprint.encrypt.DESHelper");
    // encode.encryptByPublic.overload('java.lang.String', 'java.lang.String').implementation = function (v1, v2) {
    //     v1 = v1.replaceAll("8748a6869ae34746a7de131172524417a161050708546858871","b75780863766432788e3450df0c2e1eda161288385983507278")
    //     console.log("加密数据为: " + v1);
    //     console.log("key为: " + v2)
    //     showStacks();
    //     return encode.encryptByPublic(v1,v2);
    // }


    // const en = Java.use("com.dianping.luban.k");
    // en.d.implementation = function (v1) {
    //     console.log("密钥为: " + v1);
    //     const res = en.d(v1);
    //     console.log("返回结果: " + res);
    //     return ;
    // }
    
    const api = Java.use("com.dianping.dataservice.http.a");
    api.$init.overload('java.lang.String', 'java.lang.String', 'java.io.InputStream').implementation = function (v1, v2, v3) {
        return api.a(v1, v2, v3);
    }
})