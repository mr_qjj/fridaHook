/**
 * @project: fridahook
 * @package:
 * @Description: 大用点评post数据 设备激活 验证
 * @Author: ts.
 * @Email: 
 * @Date: 2021-02-08 13:58:02
 */




/**
 * 打印堆栈信息。
 */
function showStacks() {
    Java.perform(function () {
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
    });
}


/**
 * 开始hook
 */
Java.perform(function () {

    /**
     * Hook 获取详细请求地址
     * @type {Java.Wrapper<{}>}
     */
    // const f = Java.use("com.dianping.nvnetwork.f$5");
    // f.getCommand.implementation = function (v1) {
    //     if (v1.indexOf("searchshop") > 0){
    //         console.log("当前url地址为:" + v1);
    //         showStacks();
    //     }
    //     const res =  this.getCommand(v1);
    //     return res;
    // }
    const d = Java.use("com.dianping.apimodel.BaseGetRequestBin");
    d.l_.implementation = function () {
        var res = this.l_();
        this.l_();
        this.l_();
        return res;
    }

})