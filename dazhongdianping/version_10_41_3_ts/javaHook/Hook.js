/**
 * @project: fridahook
 * @package:
 * @Description: 大用点评post数据 设备激活 验证
 * @Author: ts.
 * @Email: 
 * @Date: 2021-02-08 13:58:02
 */




/**
 * 打印堆栈信息。
 */
function showStacks() {
    Java.perform(function () {
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
    });
}


/**
 * 开始hook
 */
Java.perform(function () {

    /**
     * Hook 获取详细请求地址
     * @type {Java.Wrapper<{}>}
     */
    // const f = Java.use("com.dianping.nvnetwork.f$5");
    // f.getCommand.implementation = function (v1) {
    //     if (v1.indexOf("searchshop") > 0){
    //         console.log("当前url地址为:" + v1);
    //         showStacks();
    //     }
    //     const res =  this.getCommand(v1);
    //     return res;
    // }

    // const d = Java.use("com.dianping.model.SearchShopApiResult");
    // d.decode.implementation = function (v1) {
    //     let res = this.decode(v1);
    //     showStacks();
    //     return res;
    // }

    // let netWork = Java.use("com.dianping.nvnetwork.NVDefaultNetworkService");
    // netWork.execSync.overload('com.dianping.nvnetwork.Request').implementation = function (v1) {
    //     let res = this.execSync(v1);
    //     console.log(v1.f() + "     \t" + v1.e() + "     \t" +v1.g());
    //     return res;
    // }

    /**
     * 店铺搜索接口 请求调用了该函数, 其中 com.dianping.nvnetwork.l 类中的 g 字段为请求对象
     * 字段名称: g 对象是否为Null:false
     * 		字段名称: A	 值为: null
     * 		字段名称: b	 值为: -1
     * 		字段名称: c	 值为: false
     * 		字段名称: d	 值为: 268435393
     * 		字段名称: e	 值为: 11fcc1c025c95b40ccb68120fadeff552ea1614504148685025948d53b516146752914526f8626  //该值为 11+unionid + 随机字符串
     * 		字段名称: f	 值为: https://mapi.dianping.com/mapi/searchshop.api?islocalsearch=1&start=0&myLng=116.45464897155762&mylat=39.91114139556885&categoryid=135&locatecityid=2&cityid=2&range=-1&lng=116.45465&lat=39.91114&myacc=35.0&parentcategoryid=0&disablerewrite=0&ganextindex=0&source=dp_navigation_icon&devicelat=39.91114139556885&devicelng=116.45464897155762&keepcategory=0&tabid=-1&charactercount=20&isresearch=0&istravelsearch=0&noprofile=0&requestuuid=17b94977-d890-4f99-89bb-5593869c099e
     * 		字段名称: g	 值为: null
     * 		字段名称: h	 值为: GET
     * 		字段名称: i	 值为: {from-preload-sdk=android-1.2.71}
     * 		字段名称: j	 值为: 0
     * 		字段名称: k	 值为: null
     * 		字段名称: l	 值为: DISABLED
     * 		字段名称: m	 值为: null
     * 		字段名称: n	 值为: false
     * 		字段名称: o	 值为: false
     * 		字段名称: p	 值为: true
     * 		字段名称: q	 值为: false
     * 		字段名称: r	 值为: false
     * 		字段名称: s	 值为: false
     * 		字段名称: t	 值为: 100
     * 		字段名称: u	 值为: null
     * 		字段名称: v	 值为: GET: http://mapi.dianping.com/mapi/searchshop.api?islocalsearch=1&start=0&myLng=116.45464897155762&mylat=39.91114139556885&categoryid=135&locatecityid=2&cityid=2&range=-1&lng=116.45465&lat=39.91114&myacc=35.0&parentcategoryid=0&disablerewrite=0&ganextindex=0&source=dp_navigation_icon&devicelat=39.91114139556885&devicelng=116.45464897155762&keepcategory=0&tabid=-1&charactercount=20&isresearch=0&istravelsearch=0&noprofile=0&requestuuid=17b94977-d890-4f99-89bb-5593869c099e
     * 		字段名称: w	 值为: false
     * 		字段名称: x	 值为: null
     * 		字段名称: y	 值为: null
     * 		字段名称: z	 值为: null
     * 		字段名称: a	 值为: null
     * @type {Java.Wrapper<{}>}
     */
    // var l = Java.use('com.dianping.nvnetwork.l');
    // l.a.overload('com.dianping.nvnetwork.Request').implementation = function (req) {
    //     console.log(req.g() + "    \t" +req.e() + "\nunionID:" + req.d() + "\n headers: \t" + req.h().toString());
    //     // console.log("请求地址: "+this.h.e());
    //     if (req.e().indexOf("newreg") > 0){
    //         console.log(v1);
    //         showStacks();
    //     }
    //     return this.a(req);
    // };


    // let c = Java.use("com.dianping.base.push.pushservice.util.d")
    // c.a.overload('android.content.Context').implementation = function (v1) {
    //     let res = c.a(v1);
    //     console.log(res);
    //     return res;
    // }

    // let map = Java.use("java.util.HashMap");
    // map.put.implementation = function (v1, v2) {
    //     if (v1== "signature" || v1 == "model" || v1 == "brand"){
    //         console.log("key: " + v1 + "\tvalue: " + v2);
    //         showStacks();
    //     }
    //     return this.put(v1,v2);
    // }

    let dpObj = Java.use("com.dianping.archive.DPObject");
    dpObj.a.overload('[Lcom.dianping.archive.DPObject;', 'com.dianping.archive.c').implementation = function (v1, v2) {
        console.log(v2.class.getName());
        return dpObj.a(v1,v2)
    }

})