/**
 * ---------------------------------------
 * @DateTime : 2021-08-07 17:30:41
 * @Author   : Ts, QuJianJun
 * @FileName ：Hook.java
 * @Email    : 
 * @Description :
 * @ProductName :WebStorm
 * ---------------------------------------
 **/

/**
 * 打印堆栈信息
 */
function showStacks() {
    Java.perform(function () {
        console.log("----------------------------------------------------------------------------------------------\n");
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
        console.log("----------------------------------------------------------------------------------------------\n");
    });
}


Java.perform(function () {

    /**
     * hook url构造函数， 得到调用位置打印堆栈信息
     */
    const u = Java.use("java.net.URL")
    u.$init.overload('java.lang.String').implementation = function (v1) {
        if (v1.indexOf("mtop.youku.play.ups.appinfo.get") > 0) {
            console.log("--------------------------------------------------------------------------")
            console.log(v1)
            const security = Java.use("mtopsdk.security.c");
            security.a.overload('java.util.HashMap', 'java.util.HashMap', 'java.lang.String', 'java.lang.String', 'boolean').implementation = function (v1,v2,v3,v4,v5){
                console.log("v1: " + v1);
                console.log("v2: " + v2);
                console.log("v3: " + v3);
                console.log("v4: " + v4);
                console.log("v5: " + v5);
                const res  = this.a(v1,v2,v3,v4,v5);
                console.log("res:" + res);
                return null;
            }
            return this.$init(v1);
            console.log("--------------------------------------------------------------------------")
        }
        return this.$init(v1);
    }


    /**
     * hook 验证视频播放信息数据
     * @type {Java.Wrapper<{}>}
     */
    // const param = Java.use("com.youku.upsplayer.GetUps");
    // param.constructParams.implementation = function (v1, v2) {
    //     const j = Java.use("com.alibaba.fastjson.JSONObject");
    //     console.log("参数1: " + v1.toJSON())
    //     console.log("参数2 vid: " + v2.vid.value)
    //     console.log("参数2 ccode: " + v2.ccode.value)
    //     console.log("参数2 utid: " + v2.utid.value)
    //     console.log("参数2 showid: " + v2.showid.value)
    //     console.log("参数2 show_videoseq: " + v2.show_videoseq.value);
    //     showStacks();
    //     // return this.constructParams(v1,v2);
    // }

})