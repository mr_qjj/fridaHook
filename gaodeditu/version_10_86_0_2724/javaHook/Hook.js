/**
 * @project: fridahook
 * @package: com.autonavi.minimap
 * @Description:
 * @Author: ts.
 * @Email: 
 * @Date: 2021-06-29 18:06:05
 */



/**
 * 打印堆栈信息。
 */
function showStacks() {
    Java.perform(function () {
        console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()));
    });
}


Java.perform(function () {
    /**
     * hook系统Base64编码函数
     */
    let u = Java.use("java.util.HashMap")
    u.put.implementation = function (v1,v2){
        try{
            if(v2.toString().indexOf("B0HUKS908V") > 0){
                console.log(v1,v2)
                showStacks()
                console.log("================================================================\n")
            }
        }catch (e) {
            console.log("异常了...")
        }
        return this.put(v1,v2)
    }

})